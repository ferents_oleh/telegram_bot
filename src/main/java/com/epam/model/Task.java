package com.epam.model;

import lombok.*;

import java.util.Date;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@ToString
public class Task {
    private long id;

    private String title;

    private String description;

    private TaskStatus status;

    private Date finishDate;
}
