package com.epam.controller.impl;

import com.epam.controller.UserController;
import com.epam.dao.UserDao;
import com.epam.exception.UserAlreadyExists;
import com.epam.model.User;
import org.springframework.stereotype.Component;

@Component
public class UserControllerImpl implements UserController {
    private final UserDao userDao;

    public UserControllerImpl(UserDao userDao) {
        this.userDao = userDao;
    }

    @Override
    public int save(User user) throws UserAlreadyExists {
        if (userDao.checkIfUserExist(user.getChatId())) {
            return 0;
        }
        return userDao.save(user);
    }
}
