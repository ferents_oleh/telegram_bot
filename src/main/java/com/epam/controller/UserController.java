package com.epam.controller;

import com.epam.exception.UserAlreadyExists;
import com.epam.model.User;
import org.springframework.stereotype.Component;

@Component
public interface UserController {
    int save(User user) throws UserAlreadyExists;
}
