package com.epam.dao.impl;

import com.epam.dao.UserDao;
import com.epam.exception.UserAlreadyExists;
import com.epam.model.User;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

@Repository
public class UserDaoImpl implements UserDao {
    private final JdbcTemplate jdbcTemplate;

    public UserDaoImpl(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    @Override
    public int save(User user) {
        return jdbcTemplate.update(
                "insert into users (chat_id) values (?)",
                user.getChatId()
        );
    }

    @Override
    public boolean checkIfUserExist(long chatId) throws UserAlreadyExists {
        int count = jdbcTemplate.queryForObject(
                "select count(*) from users where users.chat_id = ?",
                new Object[] { chatId },
                Integer.class
        );
        System.out.println(count);
        boolean exist = count > 0;
        if (exist) {
            throw new UserAlreadyExists("User with such chat id already exists");
        }
        return exist;
    }
}
