package com.epam.dao;

import com.epam.exception.UserAlreadyExists;
import com.epam.model.User;

public interface UserDao {
    int save(User user);

    boolean checkIfUserExist(long chatId) throws UserAlreadyExists;
}
