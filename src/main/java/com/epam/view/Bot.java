package com.epam.view;

import com.epam.controller.UserController;
import com.epam.exception.UserAlreadyExists;
import com.epam.model.User;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.telegram.telegrambots.bots.TelegramLongPollingBot;
import org.telegram.telegrambots.meta.api.methods.send.SendMessage;
import org.telegram.telegrambots.meta.api.objects.Update;
import org.telegram.telegrambots.meta.exceptions.TelegramApiException;

@Component
public class Bot extends TelegramLongPollingBot {
    @Value("${bot.token}")
    private String botToken;

    @Value("${bot.name}")
    private String name;

    private final UserController userController;

    public Bot(UserController userController) {
        this.userController = userController;
    }

    public void onUpdateReceived(Update update) {
        if (update.hasMessage() && update.getMessage().hasText()) {
            String messageText = update.getMessage().getText();
            switch (messageText) {
                case "/start":
                    long chatId = update.getMessage().getChatId();
                    User user = new User();
                    user.setChatId(chatId);
                    try {
                        int result = userController.save(user);
                        if (result != 0) {
                            createAndExecuteSendMessage(chatId, "Welcome!");
                        }
                    } catch (UserAlreadyExists userAlreadyExists) {
                        userAlreadyExists.printStackTrace();
                    }
            }
        }
    }

    private void createAndExecuteSendMessage(long chatId, String messageText) {
        SendMessage sendMessage = new SendMessage()
                .setChatId(chatId)
                .setText(messageText);
        try {
            execute(sendMessage);
        } catch (TelegramApiException e) {
            e.printStackTrace();
        }
    }

    public String getBotUsername() {
        return name;
    }

    public String getBotToken() {
        return botToken;
    }
}
