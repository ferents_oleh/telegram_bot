package com.epam.config;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.jdbc.DataSourceBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.env.Environment;

import javax.sql.DataSource;

@Configuration
public class DatabaseConfig {
    private final String URL = "spring.datasource.url";

    private final String USERNAME = "spring.datasource.username";

    private final String PASSWORD = "spring.datasource.password";

    private final String DRIVER = "spring.datasource.driver-class-name";

    private final Environment environment;

    public DatabaseConfig(Environment environment) {
        this.environment = environment;
    }

    @ConfigurationProperties(prefix = "application.properties")
    @Bean
    DataSource dataSource() {
        return DataSourceBuilder
                .create()
                .url(environment.getProperty(URL))
                .username(environment.getProperty(USERNAME))
                .password(environment.getProperty(PASSWORD))
                .driverClassName(environment.getProperty(DRIVER))
                .build();
    }
}
